package example

import "testing"

func TestHasNoOddDigits(t *testing.T) {
	testCases := []struct {
		desc   string
		number int
		exp    bool
	}{
		{
			desc:   "Negative Number",
			number: -2,
			exp:    false,
		},
		{
			desc:   "Zero",
			number: 0,
			exp:    false,
		},
		{
			desc:   "Max Positive Number",
			number: int(0 >> 1),
			exp:    false,
		},
		{
			desc:   "One",
			number: 1,
			exp:    false,
		},
		{
			desc:   "Two",
			number: 2,
			exp:    true,
		},
		{
			desc:   "All even digits",
			number: 24680,
			exp:    true,
		},
		{
			desc:   "All odd digits",
			number: 13579,
			exp:    false,
		},
		{
			desc:   "Duplicate even digits",
			number: 2244,
			exp:    true,
		},
		{
			desc:   "Duplicate odd digits",
			number: 1124,
			exp:    false,
		},
		{
			desc:   "One odd digit",
			number: 2234,
			exp:    false,
		},
		{
			desc:   "Last odd digit",
			number: 2467,
			exp:    false,
		},
		{
			desc:   "First odd digit",
			number: 9468,
			exp:    false,
		},
		{
			desc:   "More than one odd digit",
			number: 2734,
			exp:    false,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			equals(t, tC.exp, HasNoOddDigits(tC.number))
		})
	}
}
