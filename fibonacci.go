package example

import (
	"fmt"
)

// SumFirstEvenFibonacciNumbers calculates the sum of the first limit even Fibonacci numbers.
// It is limited to maximum of 100 even numbers
func SumFirstEvenFibonacciNumbers(limit int) (sum int, err error) {
	if limit < 0 || limit > 100 {
		return 0, fmt.Errorf("Invalid limit %d", limit)
	}

	var (
		n     int = 0 // f(n) = f(n-1) + f(n-2) where n > 1
		n1    int = 1 // f(n-1), initialise to f(1)
		n2    int = 1 // f(n-2), initialise to f(0)
		count int = 0
	)

	for n = 2; count < limit; n++ {
		n = n1 + n2
		n2 = n1
		n1 = n
		if n%2 == 0 { // Even valued number
			sum += n
			count++
		}
	}
	return sum, err
}
