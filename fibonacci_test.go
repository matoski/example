package example

import (
	"testing"
)

func TestSumFirstEvenFibonacciNumbersErrors(t *testing.T) {
	testCases := []struct {
		desc  string
		limit int
	}{
		{
			desc:  "Negative Limit",
			limit: -1,
		},
		{
			desc:  "Max Limit",
			limit: 101,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			_, err := SumFirstEvenFibonacciNumbers(tC.limit)
			assert(t, err != nil, "expected an error")
		})
	}
}

func TestSumFirstEvenFibonacciNumbers(t *testing.T) {
	testCases := []struct {
		desc  string
		limit int
		exp   int
	}{
		{
			desc:  "Zero",
			limit: 0,
			exp:   0,
		},
		{
			desc:  "One",
			limit: 1,
			exp:   2,
		},
		{
			desc:  "Two",
			limit: 2,
			exp:   10,
		},
		{
			desc:  "Three",
			limit: 3,
			exp:   44,
		},
		{
			desc:  "Five",
			limit: 5,
			exp:   798,
		},
		{
			desc:  "One Hundred",
			limit: 100,
			exp:   4111518415607084508,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			act, err := SumFirstEvenFibonacciNumbers(tC.limit)
			ok(t, err)
			equals(t, tC.exp, act)
		})
	}
}
