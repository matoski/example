package example

// IntersectionOfSortedArrays returns the intersection of two sorted arrays and omits duplicate values
func IntersectionOfSortedArrays(array1 []int, array2 []int) (intersection []int) {
	var (
		len1  = len(array1)
		len2  = len(array2)
		value int
		found bool = false
	)

	for i1, i2 := 0, 0; i1 < len1 && i2 < len2; {
		if array1[i1] < array2[i2] {
			i1++
		} else if array2[i2] < array1[i1] {
			i2++
		} else { // array1[i1] == array2[i2]
			value = array1[i1]
			if !found {
				intersection = append(intersection, value)
				found = true
			} else if value != intersection[len(intersection)-1] {
				intersection = append(intersection, value)
			}
			i1++
			i2++
		}
	}
	return intersection
}
