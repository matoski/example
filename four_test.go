package example

import "testing"

func TestSumOfFourError(t *testing.T) {
	testCases := []struct {
		desc  string
		digit int
	}{
		{
			desc:  "Negative Digit",
			digit: -1,
		},
		{
			desc:  "Negative Number",
			digit: -123,
		},
		{
			desc:  "Non Digit Number",
			digit: 10,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			_, err := SumOfFour(tC.digit)
			assert(t, err != nil, "expected an error")
		})
	}
}

func TestSumOfFour(t *testing.T) {
	testCases := []struct {
		desc  string
		digit int
		exp   int
	}{
		{
			desc:  "Zero",
			digit: 0,
			exp:   0,
		},
		{
			desc:  "One",
			digit: 1,
			exp:   1234,
		},
		{
			desc:  "Two",
			digit: 2,
			exp:   2468,
		},
		{
			desc:  "Three",
			digit: 3,
			exp:   3702,
		},
		{
			desc:  "Four",
			digit: 4,
			exp:   4936,
		},
		{
			desc:  "Five",
			digit: 5,
			exp:   6170,
		},
		{
			desc:  "Six",
			digit: 6,
			exp:   7404,
		},
		{
			desc:  "Seven",
			digit: 7,
			exp:   8638,
		},
		{
			desc:  "Eight",
			digit: 8,
			exp:   9872,
		},
		{
			desc:  "Nine",
			digit: 9,
			exp:   11106,
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			act, err := SumOfFour(tC.digit)
			ok(t, err)
			equals(t, tC.exp, act)
		})
	}
}
