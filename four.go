package example

import "fmt"

// SumOfFour calculates the sum of D+DD+DDD+DDDD for the provided digit D
func SumOfFour(digit int) (sum int, err error) {
	const SumForOne int = 1234 // 1+11+111+1111

	if digit >= 0 && digit <= 9 {
		sum = SumForOne * digit
	} else {
		err = fmt.Errorf("Invalid digit %d", digit)
	}
	return sum, err
}
