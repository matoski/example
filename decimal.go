package example

// HasNoOddDigits validates if given number has no odd digits
func HasNoOddDigits(number int) bool {
	if number <= 0 {
		return false
	}

	var (
		n int = number
		d int
	)

	for n != 0 {
		d = n % 10
		n = n / 10
		if d%2 != 0 {
			return false
		}
	}
	return true
}
