package example

import "testing"

func TestIntersectionOfSortedArrays(t *testing.T) {
	testCases := []struct {
		desc   string
		array1 []int
		array2 []int
		exp    []int
	}{
		{
			desc:   "Nil",
			array1: nil,
			array2: nil,
			exp:    nil,
		},
		{
			desc:   "First Array Nil",
			array1: nil,
			array2: []int{1, 2, 3},
			exp:    nil,
		},
		{
			desc:   "Second Array Nil",
			array1: []int{1, 2, 3},
			array2: nil,
			exp:    nil,
		},
		{
			desc:   "No Intersection",
			array1: []int{1, 2, 3},
			array2: []int{4, 5, 6},
			exp:    nil,
		},
		{
			desc:   "Intersection For Same Length Arrays",
			array1: []int{1, 2, 3},
			array2: []int{2, 5, 6},
			exp:    []int{2},
		},
		{
			desc:   "Intersection When Array1 is Larger",
			array1: []int{1, 2, 3, 5, 8, 9},
			array2: []int{2, 5, 6, 9},
			exp:    []int{2, 5, 9},
		},
		{
			desc:   "Intersection When Array2 is Larger",
			array1: []int{2, 5, 6, 9},
			array2: []int{1, 2, 3, 5, 8, 9},
			exp:    []int{2, 5, 9},
		},
		{
			desc:   "Intersection Should Not Contain Duplicates",
			array1: []int{2, 2, 2, 5, 6, 9},
			array2: []int{1, 2, 2, 3, 5, 5, 8, 9},
			exp:    []int{2, 5, 9},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			equals(t, tC.exp, IntersectionOfSortedArrays(tC.array1, tC.array2))
		})
	}
}
