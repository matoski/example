package example

import (
	"flag"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

var update = flag.Bool("update", false, "update .golden files")

// expected returns the expected value from a golden file
// and updates it if the --update flag is set
func expected(tb testing.TB, actual []byte) (expected []byte) {
	var err error
	golden := filepath.Join("testdata", tb.Name()+".golden")
	if *update {
		ok(tb, ioutil.WriteFile(golden, actual, 0644))
	}
	expected, err = ioutil.ReadFile(golden)
	ok(tb, err)
	return
}

// assert fails the test if the condition is false.
func assert(tb testing.TB, condition bool, msg string, v ...interface{}) {
	if !condition {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: %s\033[39m\n\n", append([]interface{}{msg, filepath.Base(file), line}, v...)...)
		tb.FailNow()
	}
}

// ok fails the test if an err is not nil.
func ok(tb testing.TB, err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
		tb.FailNow()
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %+v\n\n\tgot: %+v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}
